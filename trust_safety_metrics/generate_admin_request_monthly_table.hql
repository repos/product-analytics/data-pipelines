--
-- Populate the source_logging_table (mediawiki_private_cu_log) monthly,
-- and aggregate the data into relevant dimensions in the destination table wmf_product.trust_safety_admin_request_monthly.
--
--
-- Parameters:
--     source_logging_table         -- Fully qualified table name of the source logging data.
--     destination_table            -- Fully qualified table name for the administrator request data.
--     canonical_table              -- Fully qualified table name for canonical wiki data
--     snapshot                     -- Snapshot of the partition to process.
--     coalesce_partitions          -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_admin_request_monthly_table.hql \
--         -d source_logging_table=wmf_raw.mediawiki_private_cu_log \
--         -d destination_table=wmf_product.trust_safety_admin_request_monthly \
--         -d canonical_table=canonical_data.wikis \
--         -d snapshot=2024-09 \
--         -d coalesce_partitions=1

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    month_time >= TO_TIMESTAMP('${snapshot}')
    AND month_time < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
;

INSERT INTO TABLE ${destination_table}
SELECT   /*+ COALESCE(${coalesce_partitions}) */
    TRUNC(TO_TIMESTAMP(cul_timestamp,'yyyyMMddHHmmss'), 'MONTH')  AS month_time,
    wiki_db,
    database_group,
    COUNT(cul_id) AS checkuser_requests
FROM ${source_logging_table}
INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
WHERE
    month = '${snapshot}'
    AND TO_TIMESTAMP(cul_timestamp,'yyyyMMddHHmmss')  >= TO_TIMESTAMP('${snapshot}')
    AND TO_TIMESTAMP(cul_timestamp,'yyyyMMddHHmmss')  < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
GROUP BY  TRUNC(TO_TIMESTAMP(cul_timestamp,'yyyyMMddHHmmss'), 'MONTH'), wiki_db, database_group
