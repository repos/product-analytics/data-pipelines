--
-- Creates a table for admin checkuser request stats, staged and partitioned monthly.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_admin_request_monthly_table.hql \
--         -d table_name=wmf_product.trust_safety_admin_request_monthly \
--         -d base_directory=/wmf/data/wmf_product/trust_safety_metrics

CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `month_time`            timestamp   COMMENT "YYYY-MM-01 00:00:00.000",
    `wiki_db`               string      COMMENT "enwiki, etc.",
    `database_group`        string      COMMENT "Project family, e.g. wikipedia",
    `checkuser_requests`    bigint      COMMENT "number of admin checkuser requests"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
