-- Purges Automoderator's monthly activity snapshots, older than the three months.
--
-- Usage:
--     spark3-sql -f purge_automoderator_activity_snapshot_monthly.hql \
--         -d source_table=wmf_product.automoderator_activity_snapshot_monthly \
--         -d snapshot=2024-08

DELETE FROM ${source_table}
WHERE snapshot <= DATE_FORMAT(ADD_MONTHS('${snapshot}', -3), 'yyyy-MM')
;
