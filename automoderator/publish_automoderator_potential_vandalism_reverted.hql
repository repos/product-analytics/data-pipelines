-- Publishes a monthly snapshot file of potential vandalism reverted by Automoderator,
-- as a CSV (but tab-seperated) to a temporary location.
-- HDFSArchiveOperator in the respective DAG will move the file from the tmp_directory,
-- to /wmf/data/published/ for web publication, as a TSV file.
--
-- Usage:
--     spark-sql -f publish_automoderator_potential_vandalism_reverted.hql \
--               -d source_table=wmf_product.automoderator_potential_vandalism_reverted \
--               -d tmp_directory=/wmf/tmp/analytics_product/automoderator/potential_vandalism_reverted \
--               -d snapshot=2024-09

INSERT OVERWRITE DIRECTORY "${tmp_directory}"
USING CSV
OPTIONS ('compression' 'bzip2', 'delimiter' '\t', 'header' 'true')
-- coalesce 1 to get a single file as the output
SELECT /*+ coalesce(1) */ 
    *
FROM
    ${source_table}
WHERE
    snapshot = '${snapshot}'
;
