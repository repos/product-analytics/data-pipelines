--
-- Creates a table to store information related to potential vandalism reverted by Automoderator, along with other user types.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_automoderator_potential_vandalism_reverted.hql \
--         -d table_name=wmf_product.automoderator_potential_vandalism_reverted \
--         -d base_directory=/wmf/data/wmf_product/automoderator/potential_vandalism_reverted

CREATE TABLE IF NOT EXISTS ${table_name} (
    `snapshot`                      string  COMMENT "Snapshot in MMMM-YY format",
    `wiki_db`                       string  COMMENT "Database code of the wiki, e.g., trwiki.",
    `is_small_wiki`                 boolean COMMENT "Indicates whether the wiki is considered a small wiki or not (within the scope of Automoderator).",
    `revision_date`                 date    COMMENT "Date on which the revision (edit) that was reverted.",
    `revert_performer_user_type`    string  COMMENT "Type of the user who made the revert, possible values: anonymous, temporary, bot, Automoderator.",
    `reverted_revision_count`       bigint  COMMENT "Number of edits reverted meeting the given criteria."
)
USING ICEBERG
PARTITIONED BY (snapshot)
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;