--
-- Creates a table for aggregated patrolled recentchanges activity on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_moderation_patrolled_recentchanges_daily.hql \
--         -d table_name=wmf_product.moderation_patrolled_recentchanges_daily \
--         -d base_directory=/wmf/data/wmf_product/moderation/patrolled_recentchanges_daily

CREATE TABLE IF NOT EXISTS ${table_name}(
    `wiki_db`                        string  COMMENT 'MediaWiki database name',
    `date`                           date    COMMENT 'Day for which the metric is computed over',
    `is_ns0`                         boolean COMMENT 'Indicates whether the log is related to namespace zero or not',
    `is_page_creation`               boolean COMMENT 'Indicates whether the log is related to a new page creation',
    `patrol_count`                   int     COMMENT 'Number of recentchanges patrolled in a day',
    `patroller_count`                int     COMMENT 'Number of unique patrollers for recentchanges in a day',
    `mean_duration_to_patrol_secs`   bigint  COMMENT 'Mean duration for recentchanges to be patrolled',
    `median_duration_to_patrol_secs` bigint  COMMENT 'Median duration for recentchanges to be patrolled'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
