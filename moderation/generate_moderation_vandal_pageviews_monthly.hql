--
-- Calculates the number of pageviews received to potentially vandalized revisions 
-- on a monthly basis, including number of revisions and pageviews to articles in namespace zero.
--
-- Parameters:
--     source_history_table     -- Fully qualified table name of the source mediawiki history table
--     source_pageviews_table   -- Fully qualified table name of the source pageviews table
--     destination_table        -- Fully qualified table name of the vandalism pageviews monthly table
--     canonical_table          -- Fully qualified table name for canonical wiki data
--     wiki_db                  -- MediaWiki database code to calculate the metrics
--     snapshot                 -- Snapshot of the partition to process
--     coalesce_partitions      -- Number of partitions to write
--
-- Usage:
--     spark3-sql -f generate_moderation_vandal_pageviews_monthly.hql \
--         -d source_history_table=wmf.mediawiki_history \
--         -d source_pageviews_table=wmf.pageview_actor \
--         -d destination_table=wmf_product.moderation_vandal_pageviews_monthly \
--         -d canonical_table=canonical_data.wikis \
--         -d wiki_db=dewiki \
--         -d snapshot=2024-06 \
--         -d coalesce_partitions=1

-- Delete existing data to prevent duplication
DELETE FROM ${destination_table}
WHERE
    month_time = TO_TIMESTAMP('${snapshot}')
    AND wiki_db = '${wiki_db}'
;

WITH base AS (
    SELECT
        wiki_db,
        revision_id,
        event_user_text,
        revision_first_identity_reverting_revision_id,
        event_timestamp,
        event_user_first_edit_timestamp
    FROM 
        ${source_history_table}
    WHERE 
        snapshot = '${snapshot}'
        AND wiki_db = '${wiki_db}'
        AND event_entity = 'revision'
        AND event_type = 'create'
        AND page_namespace_is_content
        AND (
            event_user_is_anonymous 
            OR (
                event_user_revision_count <= 15
                AND UNIX_TIMESTAMP(TO_TIMESTAMP(event_timestamp, 'yyyy-MM-dd HH:mm:ss.S')) - 
                    UNIX_TIMESTAMP(TO_TIMESTAMP(event_user_first_edit_timestamp, 'yyyy-MM-dd HH:mm:ss.S')) <= 48 * 60 * 60
            )
        )
        AND SIZE(event_user_is_bot_by_historical) = 0
        AND revision_is_identity_reverted
        AND revision_seconds_to_identity_revert <= 24 * 60 * 60
        AND revision_seconds_to_identity_revert >= 0
        AND YEAR(event_timestamp) = YEAR(DATE('${snapshot}'))
        AND MONTH(event_timestamp) = MONTH(DATE('${snapshot}'))
),

vandal_edits AS (
    SELECT
        base.wiki_db,
        base.revision_id
    FROM 
        base
    JOIN
        ${source_history_table} mwh
        ON base.wiki_db = mwh.wiki_db 
        AND base.revision_first_identity_reverting_revision_id = mwh.revision_id
    WHERE
        snapshot = '${snapshot}'
        AND NOT base.event_user_text = mwh.event_user_text
),

pv_domains AS (
    SELECT
        domain_name,
        mobile_domain_name
    FROM
        ${canonical_table}
    WHERE
        database_code = '${wiki_db}'
),

vandal_pageviews AS (
    SELECT
        1 AS view,
        ts,
        CASE
            WHEN agent_type = 'user' THEN TRUE
            ELSE FALSE
        END AS is_user,
        CASE
            WHEN access_method = 'desktop' THEN 'desktop'
            WHEN access_method LIKE '%mobile%' THEN 'mobile'
            ELSE 'misc'
        END AS platform,
        x_analytics_map['rev_id'] AS rev_id
    FROM
        ${source_pageviews_table} pva
    WHERE
        is_pageview
        AND year = YEAR(DATE('${snapshot}'))
        AND month = MONTH(DATE('${snapshot}'))
        AND uri_host IN (
            SELECT 
                domain_name 
            FROM 
                pv_domains 
            UNION ALL 
            SELECT 
                mobile_domain_name 
            FROM 
                pv_domains
        )
        AND x_analytics_map['rev_id'] IN (SELECT revision_id FROM vandal_edits)
        AND namespace_id = 0
),

agg_vandal_pvs AS (
    SELECT
        SUM(view) AS views,
        COUNT(DISTINCT rev_id) AS revisions,
        is_user,
        platform
    FROM
        vandal_pageviews
    GROUP BY
        is_user,
        platform
),

all_pageviews AS (
    SELECT
        1 AS view,
        CASE
            WHEN agent_type = 'user' THEN TRUE
            ELSE FALSE
        END AS is_user,
        CASE
            WHEN access_method = 'desktop' THEN 'desktop'
            WHEN access_method LIKE '%mobile%' THEN 'mobile'
            ELSE 'misc'
        END AS platform
    FROM
        ${source_pageviews_table} pva
    WHERE
        is_pageview
        AND year = YEAR(DATE('${snapshot}'))
        AND month = MONTH(DATE('${snapshot}'))
        AND uri_host IN (
            SELECT 
                domain_name 
            FROM 
                pv_domains 
            UNION ALL 
            SELECT 
                mobile_domain_name 
            FROM 
                pv_domains
        )
        AND namespace_id = 0
),

all_pvs AS (
    SELECT
        SUM(view) AS views,
        is_user,
        platform
    FROM
        all_pageviews
    GROUP BY
        is_user,
        platform
)

-- insert into the destination table
INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    TO_TIMESTAMP('${snapshot}') AS month_time,
    '${wiki_db}' AS wiki_db,
    vpv.is_user,
    vpv.platform,
    vpv.revisions AS potential_vandal_revision_count,
    vpv.views AS vandal_pageview_count,
    all.views AS ns0_article_pageview_count
FROM
    agg_vandal_pvs vpv
JOIN
    all_pvs all
ON vpv.is_user = all.is_user
AND vpv.platform = all.platform
;
