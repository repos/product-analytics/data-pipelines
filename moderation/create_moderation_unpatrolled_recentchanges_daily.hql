--
-- Creates a table for aggregated unpatrolled recentchanges metrics on a daily basis.
-- Note: The table also contains counts for patrolled and autopatrolled changes, for ease of use later.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_moderation_unpatrolled_recentchanges_daily.hql \
--         -d table_name=wmf_product.moderation_unpatrolled_recentchanges_daily \
--         -d base_directory=/wmf/data/wmf_product/moderation/unpatrolled_recentchanges_daily

CREATE TABLE IF NOT EXISTS ${table_name} (
    `wiki_db`           string  COMMENT 'MediaWiki database name',
    `date`              date    COMMENT 'Partition date for which the metric is computed over',
    `rc_date`           date    COMMENT 'Date for which the recentchanges status is aggregated for, usually 15 days prior to the partition date', 
    `is_ns0`            boolean COMMENT 'Indicates whether the log is on namespace zero or not',
    `is_page_creation`  boolean COMMENT 'Indicates whether the revision resulted in page creation',
    `patrol_status`     string  COMMENT 'Patrol status of the change, possible values: autopatrolled, patrolled, unpatrolled',
    `revision_count`    int     COMMENT 'Number of revisions for the given dimensions'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
