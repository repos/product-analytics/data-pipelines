-- Create table statement for citation_needed_searches_daily table.
--
-- Parameters:
--     destination_table    -- Name of the table to create
--                             (database name provided through spark3-sql
--                              command-line argument).
--     location             -- HDFS folder path to place the table files in.
--
-- Usage
--     spark3-sql -f create_citation_needed_searches_daily_table.hql   \
--     --database wmf_product                                          \
--     -d destination_table=citation_needed_searches_daily             \
--     -d location=/wmf/data/wmf_product/citation_needed_searches_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `${destination_table}`(
    `wiki_db`        string  COMMENT 'MediaWiki database code',
    `search_count`   bigint  COMMENT 'Number of Cirrus Search requests made by Citation Needed API',
    `day`            date    COMMENT 'The day for which the metric is computed over.'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${location}'
;
