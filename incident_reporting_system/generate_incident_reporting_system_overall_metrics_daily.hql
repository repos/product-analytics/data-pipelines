--
--
-- Parameters:
--     source_table         -- Fully qualified table name to compute the aggregation for.
--     destination_table    -- Fully qualified table name to fill in aggregated values.
--     coalesce_partitions  -- Number of partitions to write
--     target_year          -- Year of partition to compute aggregation for.
--     target_month         -- Month of partition to compute aggregation for.
--     target_day           -- Day of partition to compute aggregation for.
--
-- Usage:
--      spark3-sql -f generate_incident_reporting_system_incident_type_daily.hql                    \
--            -d source_table=event.mediawiki_product_metrics_incident_reporting_system_interaction \
--            -d destination_table=wmf_product.incident_reporting_system_overall_metrics_daily      \
--            -d coalesce_partitions=1                                                              \
--            -d target_year= 2025                                                                  \
--            -d target_month= 2                                                                    \
--            -d target_day= 26

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    day = TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    )
;

WITH
action_data AS (
  SELECT
    TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    ) AS date,
    CONCAT(normalized_host.project, '.', normalized_host.project_class) AS wiki_db,
    performer.session_id AS session_id,
    action,
    action_subtype,
    action_context,
    action_source,
    funnel_name,
    funnel_entry_token,
    funnel_event_sequence_position
  FROM ${source_table}
  WHERE year = ${target_year}
    AND month = ${target_month}
    AND day = ${target_day}
    AND action IN('view', 'click')
    AND normalized_host.project != 'test'
),
initial_form_interactions AS ( --- report start and choose flow
  SELECT
    date,
    wiki_db,
    action,
    funnel_entry_token,
    funnel_event_sequence_position,
    action_context
  FROM action_data
  WHERE action_source = 'form'
    AND action_subtype IS NULL
    AND (action_context IS NULL OR action_context IN('emergency', 'non-emergency'))
),
submit_page_view AS ( --- user reach submit report page for emergency flow
  SELECT
    date,
    wiki_db,
    session_id,
    funnel_entry_token
  FROM action_data
  WHERE action = 'view'
    AND action_source = 'submit_report'
    AND funnel_name = 'emergency'
),
submittd_emergency AS ( --- submitted emergency incident report
  SELECT
    date,
    wiki_db,
    session_id,
    funnel_entry_token
  FROM action_data
  WHERE action = 'view'
    AND action_source = 'submitted'
    AND funnel_name = 'emergency'
),
get_support_click AS (  --- non-emergency get support
  SELECT
    date,
    wiki_db,
    session_id,
    funnel_entry_token
  FROM action_data
  WHERE action = 'click'
    AND action_source = 'get_support'
    AND funnel_name = 'non-emergency'

),
initial_form_counts AS (
  SELECT
    date,
    wiki_db,
    SUM(IF(action = 'view' AND funnel_event_sequence_position = 1, 1, 0)) AS view_count,
    SUM(IF(action = 'click' AND action_context = 'emergency', 1, 0)) AS emergency_click_count,
    SUM(IF(action = 'click' AND action_context = 'non-emergency', 1, 0)) AS nonemergency_click_count
  FROM initial_form_interactions
  GROUP BY date, wiki_db
),
submit_page_count AS (
  SELECT
    date,
    wiki_db,
    COUNT(DISTINCT(funnel_entry_token)) AS emergency_start_submit_count
  FROM submit_page_view
  GROUP BY date, wiki_db
),
emergency_submitted_count AS (
  SELECT
    date,
    wiki_db,
    COUNT(DISTINCT(funnel_entry_token)) AS emergency_submitted_count
  FROM submittd_emergency
  GROUP BY date, wiki_db
),
non_emergency_completed_count AS (
  SELECT
    date,
    wiki_db,
    COUNT(DISTINCT(funnel_entry_token)) AS non_emergency_completed_count
  FROM get_support_click
  GROUP BY date, wiki_db
)

INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
  CAST(i.date AS DATE) AS day,
  i.wiki_db,
  view_count,
  COALESCE(emergency_click_count, 0) AS emergency_click_count,
  COALESCE(nonemergency_click_count, 0) AS nonemergency_click_count,
  COALESCE(emergency_start_submit_count, 0) AS emergency_start_submit_count,
  COALESCE(emergency_submitted_count, 0) AS emergency_submitted_count,
  COALESCE(non_emergency_completed_count, 0) AS non_emergency_completed_count
FROM initial_form_counts i
LEFT JOIN submit_page_count sp
  ON (i.date = sp.date AND i.wiki_db = sp.wiki_db)
LEFT JOIN emergency_submitted_count es
  ON (i.date = es.date AND i.wiki_db = es.wiki_db)
LEFT JOIN non_emergency_completed_count nec
  ON (i.date = nec.date AND i.wiki_db = nec.wiki_db)
;