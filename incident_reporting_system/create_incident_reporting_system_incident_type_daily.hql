--
-- Creates a table for incident types for Incident Reporting System on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_incident_reporting_system_incident_type_daily.hql \
--         -d table_name=wmf_product.incident_reporting_system_incident_type_daily \
--         -d base_directory=/wmf/data/wmf_product/incident_reporting_system/incident_type_daily



CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    day               date     COMMENT 'YYYY-MM-DD',
    wiki_db           string   COMMENT 'project name',
    funnel_name       string   COMMENT 'emergency, non-emergency',
    incident_type     string   COMMENT 'type of incident',
    incident_count    bigint   COMMENT 'number of incidents'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;

