--
-- Creates a table for overall metrics for Incident Reporting System on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_incident_reporting_system_overall_metrics_daily.hql \
--         -d table_name=wmf_product.incident_reporting_system_overall_metrics_daily \
--         -d base_directory=/wmf/data/wmf_product/incident_reporting_system/overall_metrics_daily



CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
     day                             date    COMMENT 'YYYY-MM-DD',
     wiki_db                         string  COMMENT 'project name',
     view_count                      bigint  COMMENT 'number of report started',
     emergency_click_count           bigint  COMMENT 'number of clicks on emergency report',
     nonemergency_click_count        bigint  COMMENT 'number of clicks on non-emergency report',
     emergency_start_submit_coun     bigint  COMMENT 'number of view to submit report step of emergency report',
     emergency_submitted_count       bigint  COMMENT 'number of submitted emergency report',
     non_emergency_completed_count   bigint  COMMENT 'number of completion of non-emergency flow'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;

