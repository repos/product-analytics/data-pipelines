--
--
-- Parameters:
--     source_table         -- Fully qualified table name to compute the aggregation for.
--     destination_table    -- Fully qualified table name to fill in aggregated values.
--     coalesce_partitions  -- Number of partitions to write
--     target_year          -- Year of partition to compute aggregation for.
--     target_month         -- Month of partition to compute aggregation for.
--     target_day           -- Day of partition to compute aggregation for.
--
-- Usage:
--      spark3-sql -f generate_incident_reporting_system_incident_type_daily.hql                         \
--            -d source_table=event.mediawiki_product_metrics_incident_reporting_system_interaction \
--            -d destination_table=wmf_product.incident_reporting_system_incident_type_daily        \
--            -d coalesce_partitions=1                                                              \
--            -d target_year= 2025                                                                  \
--            -d target_month= 2                                                                  \
--            -d target_day= 26

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    day = TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    )
;

-- Compute data for the period
WITH
action_data AS (
  SELECT
    TO_DATE(
        CONCAT_WS('-',
                  LPAD(${target_year}, 4, '0'),
                  LPAD(${target_month}, 2, '0'),
                  LPAD(${target_day}, 2, '0')),
        'yyyy-MM-dd'
    ) AS date,
    CONCAT(normalized_host.project, '.', normalized_host.project_class) AS wiki_db,
    performer.session_id AS session_id,
    action,
    action_subtype,
    action_context,
    action_source,
    funnel_name,
    funnel_entry_token
  FROM ${source_table}
  WHERE year = ${target_year}
    AND month = ${target_month}
    AND day = ${target_day}
    AND action IN('view', 'click')
    AND normalized_host.project != 'test'
),
describe_emergency AS ( --- selected flow and emergency incident type
  SELECT
    date,
    wiki_db,
    funnel_name,
    session_id,
    funnel_entry_token,
    GET_JSON_OBJECT(action_context, '$.harm_option') AS emergency_option
  FROM action_data
  WHERE action_source = 'form'
    AND action_subtype = 'continue'
    AND funnel_name = 'emergency'
),
describe_non_emergency AS ( --- describe non-emergency incident
  SELECT
    date,
    wiki_db,
    funnel_name,
    session_id,
    funnel_entry_token,
    action_context AS non_emergency_option
  FROM action_data
  WHERE action = 'click'
    AND action_subtype = 'continue'
    AND action_source = 'describe_unacceptable_behavior'
    AND funnel_name = 'non-emergency'
    AND action_context IS NOT NULL
),
emergency_type_count AS (
  SELECT
    date,
    wiki_db,
    funnel_name,
    emergency_option AS type,
    COUNT(DISTINCT(funnel_entry_token)) AS incident_count
  FROM describe_emergency
  GROUP BY date, wiki_db,  funnel_name, type
),
non_emergency_type_count AS (
  SELECT
    date,
    wiki_db,
    funnel_name,
    non_emergency_option AS type,
    COUNT(DISTINCT(funnel_entry_token)) AS incident_count
  FROM describe_non_emergency
  GROUP BY date, wiki_db, funnel_name, type
)

INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
  CAST(date AS DATE) AS day,
  wiki_db,
  funnel_name,
  type,
  incident_count
FROM emergency_type_count
UNION
SELECT
  CAST(date AS DATE) AS day,
  wiki_db,
  funnel_name,
  type,
  incident_count
FROM non_emergency_type_count
;